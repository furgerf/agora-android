# Agora client for the module 387com (Mobile Application Development) at Coventry University (Spring, 2015)

## Overview
This application is a mobile client for the API developed in the course of the module 305cde (Developing the modern Web 2). The API provides a platform where people can buy or sell customizable products. The name "Agora" comes from the Greek and means "gathering place". A city's agora in ancient Greece often served as a marketplace, hence the name (and "Greek theme" for the application).

In case the user is unable to log in or in any other way interact with the API, it is likely that this is because the API is running. We have purchased a Codio always-on box but we have experienced unexpected downtime. In that case, please [contact me](mailto:furgerfabian@hotmail.com).

Users can create accounts or sign in to an existing account. Once logged in, modification to the account can be made, such as changing the name of the account owner. Furthermore, all sent and received offers can be seen and their status modified (e.g. accepted).

In the main section of the application, all products can be viewed or just a subset, which is configurable with filters such as displaying only products with a certain tag, price, etc.

A product can be viewed in detail by tapping on it. If the currently logged in user is also the owner of the product, he can create an offer for a fellow Agora user who then can accept or reject it. New products can be created as well, and the user may select an image from the gallery to be uploaded for the product.

## Interesting Things
### GridView and ProductAdapter
The products are displayed in a dynamically populated GridView with a customized `BaseAdapter` called `ProductAdapter`. The purpose of this `ProductAdapter` is to be able to receive an array of products and properly display their images and names in the `GridView`.

### Images and ScaledImageView
The API sends image data in Base64-encoding so they have to be decoded first in order to create a `Bitmap` that can be displayed on the screen. When looking at a product in detail, the `ScaledImageView` is used, a class inherited from `ImageView`. The difference to the stock `ImageView` is that the aspect ratio of the image will always be preserved. Furthermore, the image is re-scaled to be as large as possible.

However, if the image is in portrait mode, the size of the image is halved so that the rest of the product information is still visible. 

The encoding/decoding for sending/receiving images is done with the help of Apache's [Commons Codec](http://commons.apache.org/proper/commons-codec/) library.

### Persistence
All _proper_ data is stored in the server where the API is running, with only one exception: If the user chooses to store the account credentials, they are stored in `SharedPreference`, which is apparently the safest way to store data on an Android device.

On a related note, it is not possible to log out of an account, with the reason being that there is usually only one user of a smartphone and that any one user should only have one account.

### Advanced REST Calls
Compared to the Quotes application, some more advanced calls to the REST API have been used. Setting headers, adding authentication, and adding a request body are examples of components that are only used in this project.

### JSON Parsing
Data is exchanged in JSON format, for which `JSONObject`s are used. A problem was presented by the API's usage of ISO8601-compliant date/time format since this could not properly be parsed by Java built-in methods. For that reason, the [Joda-Time](http://www.joda.org/joda-time/) library was used. 

### Background
The background in the application is created with a drawable (`pattern.jpg`) and a bitmap (`back.xml`) resource. The bitmap resource uses the drawable and sets the tile mode `repeat` which means that the pattern is not centered or rescaled but that it is repeated as many times as necessary to fill the view.

To fill a header- and footer-row on the various screens, a `FrameLayout` is used with `TextView`s on the top and the bottom that display the bitmap. The reason for using the `FrameLayout` is that different child views are allowed to overlap which is necessary because the background shouldn't be in the way of the actual content. 

## Reflection
This project does not use something entirely new but puts together a few nice concepts. I have been able use a variety of layouts and customized resources which makes the application more interesting.

JSON parsing (like any parsing, really) is annoying but `JSONObject`s are very easy to use and getting the advanced REST calls to work took some trial-and-error but was no major problem.

Something that slightly bothered me was that I could only start developing this application after the API was past the first major release, I would have preferred to work on it sooner to have more time for other project when the deadlines are approaching.

The application suffers somewhat from the shortcomings of the API: It was intended to provide instant messaging functionality, which is why the seller initiates offers (rather than the buyer). Since this has not been implemented on the API, the user of the application has to _know_ the buyer's email address somehow...

Overall, I am happy with this application as I think it rounds off my submission nicely.

## Limitations
The functionality of the application is of course limited by the functionality provided by the API. Furthermore, it requires an active Internet connection to be of any use.

A particular problem that was encountered is when too large images are being used (too large seems to refer to sizes above ~20kb). Tapping on a product with such a large image causes the application to crash. This is due to the limit imposed on `Intent.putExtra(...);`. There would be ways around this issue such as storing the image on the device and passing the path to the file but that has not been implemented.
