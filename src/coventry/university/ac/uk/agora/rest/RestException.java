package coventry.university.ac.uk.agora.rest;

/**
 * Class that extends exception which is used to communicate error messages
 * received from the Agora API.
 * 
 * @author fabian
 *
 */
public final class RestException extends Exception {
	private static final long serialVersionUID = 1L;

	public final String code;

	public final String message;

	public final Exception actualException;

	public boolean isRestException() {
		return actualException == null;
	}

	/**
	 * Constructor for when the RestException is actually not a RestException.
	 * 
	 * @param actualException
	 */
	public RestException(final Exception actualException) {
		super(actualException.getMessage());

		this.code = null;
		this.message = null;
		this.actualException = actualException;
	}

	/**
	 * Constructor for actual RestExceptions.
	 * 
	 * @param code
	 * @param message
	 */
	public RestException(final String code, final String message) {
		super(message);

		this.code = code;
		this.message = message;
		this.actualException = null;
	}
}
