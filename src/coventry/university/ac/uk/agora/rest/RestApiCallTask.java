package coventry.university.ac.uk.agora.rest;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

/**
 * Extends AsyncTask<...> to create async API calls. Only visible within
 * package.
 * 
 * @author fabian
 *
 */
final class RestApiCallTask extends AsyncTask<String, Void, String> {
	// constants
	private final static String TAG = RestApiCallTask.class.getSimpleName();

	// callback after API call finishes
	private final IAgoraCallback mCallback;

	// exception during API call
	private RestException mException;

	// true if the request is GET, false if POST
	private final boolean mGetRequest;

	// request body
	private StringEntity mData;

	// authentication string used in the request
	private final String mAuthentication;

	/**
	 * Constructor for authenticated GET requests.
	 * 
	 * @param callback
	 * @param username
	 * @param password
	 */
	public RestApiCallTask(final IAgoraCallback callback,
			final String username, final String password) {
		// assign fields
		mCallback = callback;
		mGetRequest = true;
		mData = null;
		if (username != null && password != null) {
			mAuthentication = Base64.encodeToString(
					(username + ":" + password).getBytes(), Base64.URL_SAFE
							| Base64.NO_WRAP);
		} else {
			mAuthentication = null;
		}
	}

	/**
	 * Constructor for unauthenticated GET requests.
	 * 
	 * @param callback
	 */
	public RestApiCallTask(final IAgoraCallback callback) {
		// overload for authenticated GET request constructor
		this(callback, null, null);
	}

	/**
	 * Constructor for authenticated POST requests
	 * 
	 * @param callback
	 * @param username
	 * @param password
	 * @param data
	 *            Request body
	 */
	public RestApiCallTask(final IAgoraCallback callback,
			final String username, final String password, final JSONObject data) {
		// assign fields
		mCallback = callback;
		if (username != null && password != null) {
			mAuthentication = Base64.encodeToString(
					(username + ":" + password).getBytes(), Base64.URL_SAFE
							| Base64.NO_WRAP);
		} else {
			mAuthentication = null;
		}
		mGetRequest = false;
		try {
			if (data == null) {
				mData = null;
			} else {
				mData = new StringEntity(data.toString(), "UTF-8");
				mData.setContentType("application/json");
			}
		} catch (UnsupportedEncodingException e) {
			Log.e(TAG, "ERROR WHILE ENCODING DATA!", e);
			mException = new RestException(e);
			mData = null;
		}
	}

	protected String doInBackground(final String... urls) {
		if (mException != null) {
			return null;
		}

		try {
			// prepare variables
			final HttpClient httpclient = new DefaultHttpClient();
			final HttpResponse response;
			String responseString = null;

			// create request
			final HttpUriRequest request;
			if (mGetRequest) {
				request = new HttpGet(urls[0]);
			} else {
				request = new HttpPost(urls[0]);
				// set data
				if (mData != null) {
					((HttpPost) request).setEntity(mData);
				}
			}
			// set authentication
			if (mAuthentication != null) {
				request.setHeader("Authorization", "Basic " + mAuthentication);
			}

			// execute request
			response = httpclient.execute(request);

			// handle response
			final StatusLine statusLine = response.getStatusLine();
			if (statusLine.getStatusCode() == HttpStatus.SC_OK
					|| statusLine.getStatusCode() == HttpStatus.SC_CREATED) {
				// success!
				final ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				responseString = out.toString();
				out.close();
			} else {
				// failure...
				final ByteArrayOutputStream out = new ByteArrayOutputStream();
				response.getEntity().writeTo(out);
				final JSONObject json = new JSONObject(out.toString());
				out.close();

				throw new RestException(json.getString("code"),
						json.getString("message"));
			}

			// return result
			return responseString;
		} catch (Exception e) {
			// put "proper" exception into RestException
			mException = e instanceof RestException ? (RestException) e
					: new RestException(e);
			return null;
		}
	}

	protected void onPostExecute(final String result) {
		// invoke callback depending on exception
		if (mException == null) {
			mCallback.onSuccess(result);
		} else {
			Log.e(TAG, "Exception in task!", mException);
			mCallback.onFailure(mException);
		}
	}
}
