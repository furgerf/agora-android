package coventry.university.ac.uk.agora.rest;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;

import org.apache.commons.codec.binary.Hex;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

/**
 * Provides easy access to make calls to the Agpra API.
 * 
 * @author fabian
 *
 */
public final class AgoraApi {
	private final static String TAG = AgoraApi.class.getSimpleName();

	// urls
	private final static String URL = "http://harvard-mister.codio.io:8888/";
	// private final static String URL = "http://invest-compact.codio.io:8000/";
	private final static String ACCOUNT_URL = URL + "account/";
	private final static String PRODUCT_URL = URL + "product/";
	private final static String OFFER_URL = URL + "offer/";

	// constructor
	private AgoraApi() {
		// don't instantiate
	}

	/********
	 * MAIN *
	 ********/
	// account
	public static void login(final IAgoraCallback callback,
			final String username, final String password) {
		Log.d(TAG, "Attempting to log in");

		getAccount(callback, username, password);
	}

	public static void getAccount(final IAgoraCallback callback,
			final String username, final String password) {
		Log.d(TAG, "Retrieving account");

		new RestApiCallTask(callback, username, password).execute(ACCOUNT_URL
				+ getSha1Hash(username));
	}

	public static void createAccount(final IAgoraCallback callback,
			final String username, final String password) {
		Log.d(TAG, "Creating account " + username);

		new RestApiCallTask(callback, username, password, null)
				.execute(ACCOUNT_URL);
	}

	public static void updateAccount(final IAgoraCallback callback,
			final String username, final String password, final JSONObject data) {
		Log.d(TAG, "Updating account " + username);

		new RestApiCallTask(callback, username, password, data)
				.execute(ACCOUNT_URL + getSha1Hash(username));
	}

	// product
	public static void getAllProducts(final IAgoraCallback callback) {
		Log.d(TAG, "Retrieving products");

		new RestApiCallTask(callback).execute(PRODUCT_URL);
	}

	public static void getAllProducts(final IAgoraCallback callback,
			final JSONObject json) {
		Log.d(TAG, "Retrieving filtered products");

		try {
			String queryString = "?expand=true&detailed=true";
			final Iterator<?> keys = json.keys();
			while (keys.hasNext()) {
				final String key = (String) keys.next();
				queryString += "&" + key + "=" + json.getString(key);
			}
			new RestApiCallTask(callback).execute(PRODUCT_URL + queryString);
		} catch (JSONException e) {
			Log.e(TAG, "Error while parsing query string parameters", e);
		}
	}

	public static void createProduct(final IAgoraCallback callback,
			final String username, final String password, final JSONObject data) {
		Log.d(TAG, "Creating product");

		new RestApiCallTask(callback, username, password, data)
				.execute(PRODUCT_URL);
	}

	// offer
	public static void createOffer(final IAgoraCallback callback,
			final String username, final String password, final JSONObject data) {
		Log.d(TAG, "Creating offer");

		new RestApiCallTask(callback, username, password, data)
				.execute(OFFER_URL);
	}

	public static void getAllOffers(final IAgoraCallback callback,
			final String username, final String password, final JSONObject json) {
		Log.d(TAG, "Retrieving filtered offers");

		try {
			String queryString = "?expand=true";
			final Iterator<?> keys = json.keys();
			while (keys.hasNext()) {
				final String key = (String) keys.next();
				queryString += "&" + key + "=" + json.getString(key);
			}
			new RestApiCallTask(callback, username, password).execute(OFFER_URL
					+ queryString);
		} catch (JSONException e) {
			Log.e(TAG, "Error while parsing query string parameters", e);
		}
	}

	public static void updateOffer(IAgoraCallback callback, String username,
			String password, String offer, JSONObject data) {
		Log.d(TAG, "Updating offer");

		new RestApiCallTask(callback, username, password, data)
				.execute(OFFER_URL + offer);

	}

	/**********
	 * HELPER *
	 **********/
	public static String getSha1Hash(final String string) {
		try {
			final MessageDigest d = java.security.MessageDigest
					.getInstance("SHA-1");
			d.reset();
			d.update(string.getBytes());
			return new String(Hex.encodeHex(d.digest()));
		} catch (NoSuchAlgorithmException e) {
			Log.e(TAG, "ERROR while calculating SHA-1", e);
			return null;
		}
	}
}
