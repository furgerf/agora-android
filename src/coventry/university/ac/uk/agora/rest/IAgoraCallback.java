package coventry.university.ac.uk.agora.rest;

/**
 * Interface that is used for async API calls
 * 
 * @author fabian
 *
 */
public interface IAgoraCallback {
	/**
	 * Function that is called if the call to the Agora API was successful.
	 * 
	 * @param response
	 *            The response received from the API
	 */
	void onSuccess(final String response);

	/**
	 * Function that is called if the call to the Agora API was unsuccessful.
	 * 
	 * @param exception
	 *            Exception that was received, either an actual RestException if
	 *            something went wrong when in regard to the API or another
	 *            exception if there was a technical problem
	 */
	void onFailure(final RestException exception);
}
