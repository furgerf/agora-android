package coventry.university.ac.uk.agora.activity;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import coventry.university.ac.uk.R;
import coventry.university.ac.uk.agora.common.Settings;
import coventry.university.ac.uk.agora.documents.Product;
import coventry.university.ac.uk.agora.rest.AgoraApi;
import coventry.university.ac.uk.agora.rest.IAgoraCallback;
import coventry.university.ac.uk.agora.rest.RestException;

/**
 * Activity that shows a product.
 * 
 * @author fabian
 *
 */
public final class ViewProductActivity extends Activity {
	private final static String TAG = ViewProductActivity.class.getSimpleName();

	// views
	private TextView mTxtName;
	private TextView mTxtDescription;
	private TextView mTxtTags;
	private TextView mTxtPrice;
	private ImageView mProductImage;
	private Button mButCreateOffer;

	private Product mProduct;

	// callback
	private final IAgoraCallback mCreateOfferCallback = new IAgoraCallback() {
		@Override
		public void onSuccess(String response) {
			Log.i(TAG, "Offer created");
			Toast.makeText(MainActivity.getInstance(),
					"The offer was created successfully!", Toast.LENGTH_SHORT)
					.show();

			finish();
		}

		@Override
		public void onFailure(RestException exception) {
			if (exception.isRestException()
					&& exception.code.equals("InvalidProperties")) {
				Log.i(TAG,
						"Offer couldn't be created because properties are invalid");
				Toast.makeText(MainActivity.getInstance(),
						"Please provide valid data", Toast.LENGTH_SHORT).show();
			} else {
				Log.e(TAG, "Error while creating offer", exception);
				Toast.makeText(
						getApplicationContext(),
						"Something unexpected happened, please try again later",
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_product);

		// assign views
		mTxtName = (TextView) findViewById(R.id.name);
		mTxtDescription = (TextView) findViewById(R.id.description);
		mTxtTags = (TextView) findViewById(R.id.tags);
		mTxtPrice = (TextView) findViewById(R.id.price);
		mProductImage = (ImageView) findViewById(R.id.imgProduct);
		mButCreateOffer = (Button) findViewById(R.id.butCreateOffer);

		final Intent intent = getIntent();
		try {
			// get product
			mProduct = Product.parseJson(new JSONObject(intent.getExtras()
					.getString("product")));

			// fill views with data
			if (mProduct.images != null && mProduct.images.length > 0) {
				final byte[] decodedByte = Base64.decode(mProduct.images[0], 0);
				mProductImage.setImageBitmap(BitmapFactory.decodeByteArray(
						decodedByte, 0, decodedByte.length));
			} else {
				mProductImage.setImageResource(R.drawable.agora_logo);
			}
			mTxtName.setText(mProduct.name == null ? "" : mProduct.name);
			if (mProduct.description == null) {
				mTxtDescription.setText("");
				mTxtDescription.setHeight(0);
				mTxtDescription.setPadding(0, 0, 0, 0);
			} else {
				mTxtDescription.setText(mProduct.description);
			}
			if (mProduct.tags == null) {
				mTxtTags.setText("");
				mTxtTags.setHeight(0);
				mTxtTags.setPadding(0, 0, 0, 0);
			} else {
				String tagString = "";
				for (final String tag : mProduct.tags) {
					tagString += ", " + tag;
				}
				mTxtTags.setText(tagString.substring(2));
			}
			if (mProduct.minPrice != null && mProduct.maxPrice != null) {
				if (mProduct.minPrice.equals(mProduct.maxPrice)) {
					mTxtPrice.setText("Price: " + mProduct.minPrice);
				} else {
					mTxtPrice.setText("Price: from " + mProduct.minPrice
							+ " to " + mProduct.maxPrice);
				}
			} else {
				mTxtPrice.setText("");
			}

			// hide createOffer button if the current user is the owner
			if (!mProduct.isOwnedByCurrentAccount()) {
				mButCreateOffer.setVisibility(View.GONE);
			}
		} catch (JSONException e) {
			Log.e(TAG, "Yet Another JsonExceptionHandler...", e);
		}

		Log.d(TAG, TAG + " created.");
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_product, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_main_view) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void back(final View view) {
		finish();
	}

	public void createOffer(final View view) {
		final EditText txtBuyer = new EditText(this);
		txtBuyer.setInputType(InputType.TYPE_CLASS_TEXT
				| InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		new AlertDialog.Builder(this)
				.setTitle("Buyer")
				.setMessage("Please insert the name of the buyer:")
				.setView(txtBuyer)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						createOfferWithBuyer(AgoraApi.getSha1Hash(txtBuyer
								.getText().toString()));
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// do nothing
							}
						}).show();
	}

	private void createOfferWithBuyer(final String buyer) {
		final EditText txtPrice = new EditText(this);
		txtPrice.setInputType(InputType.TYPE_CLASS_NUMBER
				| InputType.TYPE_NUMBER_FLAG_DECIMAL);

		new AlertDialog.Builder(this)
				.setTitle("Price")
				.setMessage(
						mProduct.minPrice.equals(mProduct.maxPrice) ? "Set price ("
								+ mProduct.minPrice + ")"
								: "Set price (" + mProduct.minPrice + " - "
										+ mProduct.maxPrice + ")")
				.setView(txtPrice)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						createOfferWithBuyerAndPrice(buyer, Double
								.parseDouble(txtPrice.getText().toString()));
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// do nothing
							}
						}).show();
	}

	private void createOfferWithBuyerAndPrice(final String buyer,
			final double price) {
		final EditText txtDescription = new EditText(this);
		new AlertDialog.Builder(this)
				.setTitle("Description")
				.setMessage("Please add a description for the offer")
				.setView(txtDescription)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						try {
							final JSONObject json = new JSONObject();
							json.put("seller", AgoraApi.getSha1Hash(Settings
									.getUsername()));
							json.put("buyer", buyer);
							json.put("description", txtDescription.getText()
									.toString());
							json.put("price", price);
							json.put("product", mProduct._id);

							AgoraApi.createOffer(mCreateOfferCallback,
									Settings.getUsername(),
									Settings.getPassword(), json);
						} catch (JSONException e) {
							Log.e(TAG, "Another JSON exception", e);
						}

					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// do nothing
							}
						}).show();
	}
}
