package coventry.university.ac.uk.agora.activity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import coventry.university.ac.uk.R;
import coventry.university.ac.uk.agora.common.Settings;
import coventry.university.ac.uk.agora.documents.Document;
import coventry.university.ac.uk.agora.rest.AgoraApi;
import coventry.university.ac.uk.agora.rest.IAgoraCallback;
import coventry.university.ac.uk.agora.rest.RestException;

/**
 * Activity that allows an user to create a new product.
 * 
 * @author fabian
 *
 */
public final class CreateProductActivity extends Activity {
	private final static String TAG = CreateProductActivity.class
			.getSimpleName();

	private final static int GALLERY_REQUEST_CODE = 1;

	// views
	private EditText mTxtName;
	private EditText mTxtDescription;
	private TextView mTagsText;
	private EditText mTxtTags;
	private EditText mTxtMinPrice;
	private EditText mTxtMaxPrice;
	private ImageView mProductImage;
	private TextView mImageText;

	// other data container
	private final List<String> mTags = new ArrayList<String>();
	private Bitmap mImage;

	// callback
	private final IAgoraCallback mCreateProductCallback = new IAgoraCallback() {
		@Override
		public void onSuccess(String response) {
			Log.i(TAG, "Product created");
			Toast.makeText(MainActivity.getInstance(),
					"The product was created successfully!", Toast.LENGTH_SHORT)
					.show();

			MainActivity.getInstance().loadProducts();

			finish();
		}

		@Override
		public void onFailure(RestException exception) {
			if (exception.isRestException()
					&& exception.code.equals("InvalidProperties")) {
				Log.i(TAG,
						"Product couldn't be created because properties are invalid");
				Toast.makeText(MainActivity.getInstance(),
						"Please provide valid data", Toast.LENGTH_SHORT).show();
			} else {
				Log.e(TAG, "Error while creating product", exception);
				Toast.makeText(
						getApplicationContext(),
						"Something unexpected happened, please try again later",
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_product);

		// assign views
		mTxtName = (EditText) findViewById(R.id.txtName);
		mTxtDescription = (EditText) findViewById(R.id.txtDescription);
		mTagsText = (TextView) findViewById(R.id.tags);
		mTxtTags = (EditText) findViewById(R.id.txtTags);
		mTxtMinPrice = (EditText) findViewById(R.id.txtMinPrice);
		mTxtMaxPrice = (EditText) findViewById(R.id.txtMaxPrice);
		mProductImage = (ImageView) findViewById(R.id.imgProduct);
		mImageText = (TextView) findViewById(R.id.txtImageText);

		Log.d(TAG, TAG + " created.");
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_product, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_main_view) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void createProduct(final View view) {
		// validate fields
		if (mTxtName.getText().length() == 0) {
			Toast.makeText(getApplicationContext(), "Please specify a name",
					Toast.LENGTH_SHORT).show();
			return;
		}

		try {
			Double minPrice = Double.parseDouble(mTxtMinPrice.getText()
					.toString());
			Double maxPrice = Double.parseDouble(mTxtMaxPrice.getText()
					.toString());

			try {
				final JSONObject json = new JSONObject();

				// required fields
				json.put("name", mTxtName.getText().toString());
				json.put("minPrice", minPrice);
				json.put("maxPrice", maxPrice);

				// other fields
				if (mTags.size() > 0) {
					json.put("tags", Document.stringListToJsonArray(mTags));
				}
				if (mTxtDescription.getText().length() > 0) {
					json.put("description", mTxtDescription.getText()
							.toString());
				}
				if (mImage != null) {
					final ByteArrayOutputStream stream = new ByteArrayOutputStream();
					mImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
					final byte[] byteArray = stream.toByteArray();
					stream.close();

					final String encodedImage = Base64.encodeToString(
							byteArray, Base64.DEFAULT).replace("\n", "");
					final JSONArray array = new JSONArray();
					array.put(encodedImage);
					json.put("images", array);
				}

				Log.i(TAG, "Trying to create product...");

				AgoraApi.createProduct(mCreateProductCallback,
						Settings.getUsername(), Settings.getPassword(), json);
			} catch (IOException ee) {
				Log.e(TAG, "IO exception", ee);
			} catch (JSONException ee) {
				Log.e(TAG, "JSON exception", ee);
			}
		} catch (NumberFormatException e) {
			Toast.makeText(getApplicationContext(),
					"Please specify a valid min and max price",
					Toast.LENGTH_SHORT).show();
		}
	}

	public void changeImage(final View view) {
		final Intent pickImageIntent = new Intent(Intent.ACTION_PICK);
		pickImageIntent.setType("image/*");
		startActivityForResult(pickImageIntent, GALLERY_REQUEST_CODE);
	}

	public void addTag(final View view) {
		if (mTxtTags.getText().length() == 0) {
			return;
		}
		mTags.add(mTxtTags.getText().toString());
		mTxtTags.setText("");
		updateTagsText();
	}

	public void clearTags(final View view) {
		mTags.clear();
		updateTagsText();
	}

	private void updateTagsText() {
		String tags = "";
		for (int i = 0; i < mTags.size(); i++) {
			tags += ", " + mTags.get(i);
		}
		mTagsText.setText(tags.length() > 2 ? "Tags: " + tags.substring(2)
				: "Tags:");
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == GALLERY_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				try {
					final InputStream stream = getContentResolver()
							.openInputStream(data.getData());
					final Bitmap bitmap = BitmapFactory.decodeStream(stream);
					stream.close();
					mProductImage.setImageBitmap(bitmap);
					mImage = bitmap;
				} catch (Exception e) {
					e.printStackTrace();
				}

				mImageText.setText("");

				Log.i(TAG, "Received image from gallery");
			}
		} else {
			// let parent handle the activity result
			super.onActivityResult(requestCode, resultCode, data);
		}
	}
}
