package coventry.university.ac.uk.agora.activity;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import coventry.university.ac.uk.R;
import coventry.university.ac.uk.agora.common.Settings;
import coventry.university.ac.uk.agora.documents.Account;
import coventry.university.ac.uk.agora.documents.Offer;
import coventry.university.ac.uk.agora.documents.OfferStatus;
import coventry.university.ac.uk.agora.rest.AgoraApi;
import coventry.university.ac.uk.agora.rest.IAgoraCallback;
import coventry.university.ac.uk.agora.rest.RestException;

/**
 * Activity that shows an overview of the account and allows the user to change
 * values.
 * 
 * @author fabian
 *
 */
public final class AccountActivity extends Activity {
	private final static String TAG = AccountActivity.class.getSimpleName();

	// views
	private TextView mEmail;
	private EditText mFirstName;
	private EditText mLastName;
	private Spinner mSubscriptionStatus;
	private TextView mSubscriptionExpiration;
	private RelativeLayout mLayCreatedOffers;
	private RelativeLayout mLayReceivedOffers;

	// callbacks
	private IAgoraCallback mGetCreatedOffersCallback = new IAgoraCallback() {
		@Override
		public void onSuccess(final String response) {
			try {
				displayOffers(Offer.parseOffers(new JSONObject(response)
						.getJSONObject("data")));
			} catch (JSONException e) {
				Log.e(TAG, "Yet Another JSON exception...", e);
			}
		}

		@Override
		public void onFailure(final RestException exception) {
			Toast.makeText(getApplication(), "Could not retrieve offers",
					Toast.LENGTH_SHORT).show();
			Log.e(TAG, "Couldn't retrieve offers", exception);
		}
	};
	private IAgoraCallback mGetReceivedOffersCallback = new IAgoraCallback() {
		@Override
		public void onSuccess(final String response) {
			try {
				displayOffers(Offer.parseOffers(new JSONObject(response)
						.getJSONObject("data")));
			} catch (JSONException e) {
				Log.e(TAG, "Yet Another JSON exception...", e);
			}
		}

		@Override
		public void onFailure(final RestException exception) {
			Toast.makeText(getApplication(), "Could not retrieve offers",
					Toast.LENGTH_SHORT).show();
			Log.e(TAG, "Couldn't retrieve offers", exception);
		}
	};

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account);

		// assign views
		mEmail = (TextView) findViewById(R.id.txtHeader);
		mFirstName = (EditText) findViewById(R.id.txtName);
		mLastName = (EditText) findViewById(R.id.txtDescription);
		mSubscriptionStatus = (Spinner) findViewById(R.id.spiSubscriptionStatus);
		mSubscriptionExpiration = (TextView) findViewById(R.id.txtExipration);
		mLayCreatedOffers = (RelativeLayout) findViewById(R.id.layCreatedOffers);
		mLayReceivedOffers = (RelativeLayout) findViewById(R.id.layReceivedOffers);

		try {
			AgoraApi.getAllOffers(
					mGetCreatedOffersCallback,
					Settings.getUsername(),
					Settings.getPassword(),
					new JSONObject("{\"exactSeller\":\""
							+ AgoraApi.getSha1Hash(Settings.getUsername())
							+ "\"}"));
			AgoraApi.getAllOffers(
					mGetReceivedOffersCallback,
					Settings.getUsername(),
					Settings.getPassword(),
					new JSONObject("{\"exactBuyer\":\""
							+ AgoraApi.getSha1Hash(Settings.getUsername())
							+ "\"}"));
			Log.d(TAG, TAG + " created.");
		} catch (JSONException e) {
			Log.e(TAG, "Yet Another JSON exception...", e);
		} finally {
			// clear offers since we're retrieving up to date ones
			clearOffers();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		// update view values (they may have been changed by the last update)
		mEmail.setText(MainActivity.getInstance().currentAccount.email == null ? ""
				: MainActivity.getInstance().currentAccount.email);
		mFirstName
				.setText(MainActivity.getInstance().currentAccount.firstName == null ? ""
						: MainActivity.getInstance().currentAccount.firstName);
		mLastName
				.setText(MainActivity.getInstance().currentAccount.lastName == null ? ""
						: MainActivity.getInstance().currentAccount.lastName);
		mSubscriptionStatus
				.setSelection(MainActivity.getInstance().currentAccount.subscriptionStatus == null ? 0
						: MainActivity.getInstance().currentAccount.subscriptionStatus
								.getValue());
		mSubscriptionExpiration
				.setText(MainActivity.getInstance().currentAccount.subscriptionExpiration == null ? "never"
						: DateFormat
								.getMediumDateFormat(MainActivity.getInstance())
								.format(MainActivity.getInstance().currentAccount.subscriptionExpiration));
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_account, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_main_view) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void updateAccount(final View view) {
		final JSONObject changes = getAccountChanges();
		if (changes == null) {
			Toast.makeText(
					getApplicationContext(),
					"Something went wrong when preparing to update account, aborting...",
					Toast.LENGTH_SHORT).show();
		} else if (changes.length() == 0) {
			Toast.makeText(getApplicationContext(), "No changes were made",
					Toast.LENGTH_SHORT).show();
		} else {
			Log.i(TAG, "Attempting to update account with " + changes);
			AgoraApi.updateAccount(new IAgoraCallback() {
				@Override
				public void onSuccess(String response) {
					Toast.makeText(getApplicationContext(),
							"Account updated successfully", Toast.LENGTH_SHORT)
							.show();

					try {
						MainActivity.getInstance().currentAccount = Account
								.parseJson(new JSONObject(response)
										.getJSONObject("data"));
					} catch (JSONException e) {
						Log.e(TAG, "Error parsing json", e);
					}

					finish();
				}

				@Override
				public void onFailure(RestException exception) {
					Toast.makeText(
							getApplicationContext(),
							"Error while updating account: "
									+ exception.message, Toast.LENGTH_SHORT)
							.show();

				}
			}, Settings.getUsername(), Settings.getPassword(), changes);
		}
	}

	private JSONObject getAccountChanges() {
		try {
			final JSONObject json = new JSONObject();
			if (!mFirstName
					.getText()
					.toString()
					.equals(MainActivity.getInstance().currentAccount.firstName)) {
				json.put("firstName", mFirstName.getText().toString());
			}
			if (!mLastName.getText().toString()
					.equals(MainActivity.getInstance().currentAccount.lastName)) {
				json.put("lastName", mLastName.getText().toString());
			}
			if (MainActivity.getInstance().currentAccount.subscriptionStatus
					.getValue() != mSubscriptionStatus
					.getSelectedItemPosition()) {
				final JSONObject subscription = new JSONObject();
				subscription
						.put("status",
								mSubscriptionStatus
										.getSelectedItem()
										.toString()
										.toLowerCase(
												getResources()
														.getConfiguration().locale));
				json.put("subscription", subscription);
			}
			return json;
		} catch (JSONException e) {
			Log.e(TAG, "Error while creating account changes object", e);
			return null;
		}
	}

	private void clearOffers() {
		mLayReceivedOffers.removeAllViews();
		mLayCreatedOffers.removeAllViews();
	}

	private void displayOffers(final Offer[] offers) {
		if (offers == null || offers.length == 0) {
			return;
		}

		if (AgoraApi.getSha1Hash(Settings.getUsername()).equals(
				offers[0].seller)) {
			displayCreatedOffers(offers);
		} else {
			displayReceivedOffers(offers);
		}
	}

	private void displayCreatedOffers(final Offer[] offers) {
		final int marginTop = ((RelativeLayout.LayoutParams) mSubscriptionExpiration
				.getLayoutParams()).topMargin;

		// the offers made by me
		int id = 0;

		// create header
		final RelativeLayout.LayoutParams headerParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		final TextView header = new TextView(this);
		header.setText("Sent offers");
		header.setTextSize(16);
		header.setId(id++);
		mLayCreatedOffers.addView(header, headerParams);

		// modify root layout
		final RelativeLayout.LayoutParams layoutParams = (LayoutParams) mLayCreatedOffers
				.getLayoutParams();
		layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin
				+ marginTop, layoutParams.rightMargin,
				layoutParams.bottomMargin);
		mLayCreatedOffers.setLayoutParams(layoutParams);

		int lastRowId = header.getId();

		// add all offers
		for (final Offer offer : offers) {
			// offer description
			final TextView description = new TextView(this);
			description.setText("Status: " + offer.status + "\n"
					+ offer.product + "\nPrice: £" + offer.price
					+ "\nDescription: " + offer.description);
			description.setId(id++);

			// offer button
			final Button withdraw = new Button(this);
			withdraw.setText("Withdraw");
			withdraw.setId(id++);
			withdraw.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(final View v) {
					try {
						AgoraApi.updateOffer(
								new IAgoraCallback() {
									@Override
									public void onSuccess(String response) {
										Toast.makeText(getApplicationContext(),
												"Offer has been withdrawn",
												Toast.LENGTH_SHORT).show();

										// reload offers
										try {
											AgoraApi.getAllOffers(
													mGetCreatedOffersCallback,
													Settings.getUsername(),
													Settings.getPassword(),
													new JSONObject(
															"{\"exactSeller\":\""
																	+ AgoraApi
																			.getSha1Hash(Settings
																					.getUsername())
																	+ "\"}"));
										} catch (JSONException e) {
											Log.e(TAG, "JSONException", e);
										}
									}

									@Override
									public void onFailure(
											RestException exception) {
										Toast.makeText(getApplicationContext(),
												"Offer could not be withdrawn",
												Toast.LENGTH_SHORT).show();
										Log.e(TAG,
												"Error while updating offer",
												exception);
									}
								}, Settings.getUsername(), Settings
										.getPassword(),
								offer._id, new JSONObject(
										"{\"status\":\"withdrawn\"}"));
					} catch (JSONException e) {
						Log.e(TAG, "JSONException", e);
					}
				}
			});

			// layout
			final RelativeLayout.LayoutParams descriptionParams = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			descriptionParams.addRule(RelativeLayout.BELOW, lastRowId);
			descriptionParams
					.addRule(RelativeLayout.START_OF, withdraw.getId());
			descriptionParams.setMargins(descriptionParams.leftMargin
					+ marginTop, descriptionParams.topMargin + marginTop,
					descriptionParams.rightMargin,
					descriptionParams.bottomMargin);
			final RelativeLayout.LayoutParams withdrawParams = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			withdrawParams.addRule(RelativeLayout.ALIGN_TOP,
					description.getId());

			descriptionParams.addRule(RelativeLayout.ALIGN_PARENT_START);
			withdrawParams.addRule(RelativeLayout.ALIGN_PARENT_END);

			// add views
			if (offer.status == OfferStatus.Offered) {
				mLayCreatedOffers.addView(withdraw, withdrawParams);
			}
			mLayCreatedOffers.addView(description, descriptionParams);

			lastRowId = description.getId();
		}
	}

	private void displayReceivedOffers(final Offer[] offers) {
		final int marginTop = ((RelativeLayout.LayoutParams) mSubscriptionExpiration
				.getLayoutParams()).topMargin;

		// the offers made by someone else
		int id = 0;

		// create header
		final RelativeLayout.LayoutParams headerParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		final TextView header = new TextView(this);
		header.setText("Received offers");
		header.setTextSize(16);
		header.setId(id++);
		mLayReceivedOffers.addView(header, headerParams);

		// modify root layout
		final RelativeLayout.LayoutParams layoutParams = (LayoutParams) mLayReceivedOffers
				.getLayoutParams();
		layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin
				+ marginTop, layoutParams.rightMargin,
				layoutParams.bottomMargin);
		mLayReceivedOffers.setLayoutParams(layoutParams);

		int lastRowId = header.getId();

		// add all offers
		for (final Offer offer : offers) {
			// offer description
			final TextView description = new TextView(this);
			description.setText("Status: " + offer.status + "\n"
					+ offer.product + "\nPrice: £" + offer.price
					+ "\nDescription: " + offer.description);
			description.setId(id++);

			// buttons
			final Button accept = new Button(this);
			accept.setText("Accept");
			accept.setId(id++);
			accept.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(final View v) {
					try {
						AgoraApi.updateOffer(
								new IAgoraCallback() {
									@Override
									public void onSuccess(String response) {
										Toast.makeText(getApplicationContext(),
												"Offer has been accepted",
												Toast.LENGTH_SHORT).show();

										// reload offers
										try {
											AgoraApi.getAllOffers(
													mGetReceivedOffersCallback,
													Settings.getUsername(),
													Settings.getPassword(),
													new JSONObject(
															"{\"exactBuyer\":\""
																	+ AgoraApi
																			.getSha1Hash(Settings
																					.getUsername())
																	+ "\"}"));
										} catch (JSONException e) {
											Log.e(TAG, "JSONException", e);
										}
									}

									@Override
									public void onFailure(
											RestException exception) {
										Log.e(TAG,
												"Error while updating offer",
												exception);

										Toast.makeText(getApplicationContext(),
												"Offer could not be accepted",
												Toast.LENGTH_SHORT).show();
									}
								}, Settings.getUsername(), Settings
										.getPassword(),
								offer._id, new JSONObject(
										"{\"status\":\"accepted\"}"));
					} catch (JSONException e) {
						Log.e(TAG, "JSONException", e);
					}
				}
			});

			final Button reject = new Button(this);
			reject.setText("Reject");
			reject.setId(id++);
			reject.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(final View v) {
					try {
						AgoraApi.updateOffer(
								new IAgoraCallback() {
									@Override
									public void onSuccess(String response) {
										Toast.makeText(getApplicationContext(),
												"Offer has been rejected",
												Toast.LENGTH_SHORT).show();

										// reload offers
										try {
											AgoraApi.getAllOffers(
													mGetReceivedOffersCallback,
													Settings.getUsername(),
													Settings.getPassword(),
													new JSONObject(
															"{\"exactSeller\":\""
																	+ AgoraApi
																			.getSha1Hash(Settings
																					.getUsername())
																	+ "\"}"));
										} catch (JSONException e) {
											Log.e(TAG, "JSONException", e);
										}
									}

									@Override
									public void onFailure(
											RestException exception) {
										Log.e(TAG,
												"Error while updating offer",
												exception);
										Toast.makeText(getApplicationContext(),
												"Offer could not be rejected",
												Toast.LENGTH_SHORT).show();
									}
								}, Settings.getUsername(), Settings
										.getPassword(),
								offer._id, new JSONObject(
										"{\"status\":\"rejected\"}"));
					} catch (JSONException e) {
						Log.e(TAG, "JSONException", e);
					}
				}
			});

			// layout
			final RelativeLayout.LayoutParams descriptionParams = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			descriptionParams.addRule(RelativeLayout.BELOW, lastRowId);
			descriptionParams.addRule(RelativeLayout.START_OF, accept.getId());
			descriptionParams.setMargins(descriptionParams.leftMargin
					+ marginTop, descriptionParams.topMargin + marginTop,
					descriptionParams.rightMargin,
					descriptionParams.bottomMargin);
			final RelativeLayout.LayoutParams acceptParams = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			acceptParams.addRule(RelativeLayout.ALIGN_TOP, description.getId());

			final RelativeLayout.LayoutParams rejectParams = new RelativeLayout.LayoutParams(
					RelativeLayout.LayoutParams.WRAP_CONTENT,
					RelativeLayout.LayoutParams.WRAP_CONTENT);
			rejectParams.addRule(RelativeLayout.BELOW, accept.getId());

			descriptionParams.addRule(RelativeLayout.ALIGN_PARENT_START);
			acceptParams.addRule(RelativeLayout.ALIGN_PARENT_END);
			rejectParams.addRule(RelativeLayout.ALIGN_PARENT_END);

			// add views
			if (offer.status == OfferStatus.Offered) {
				mLayReceivedOffers.addView(accept, acceptParams);
				mLayReceivedOffers.addView(reject, rejectParams);
			}
			mLayReceivedOffers.addView(description, descriptionParams);

			lastRowId = description.getId();
		}
	}
}
