package coventry.university.ac.uk.agora.activity;

import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;
import coventry.university.ac.uk.R;
import coventry.university.ac.uk.agora.common.ProductAdapter;
import coventry.university.ac.uk.agora.common.Settings;
import coventry.university.ac.uk.agora.documents.Account;
import coventry.university.ac.uk.agora.documents.Product;
import coventry.university.ac.uk.agora.documents.ProductFilterType;
import coventry.university.ac.uk.agora.rest.AgoraApi;
import coventry.university.ac.uk.agora.rest.IAgoraCallback;
import coventry.university.ac.uk.agora.rest.RestException;

/**
 * Main activity. Shows available products.
 * 
 * @author fabian
 *
 */
public final class MainActivity extends Activity {
	private final static String TAG = MainActivity.class.getSimpleName();

	// account of the currently logged in user
	public Account currentAccount;

	private GridView mProductGrid;
	private ProductAdapter mProductAdapter;

	private JSONObject mFilter = new JSONObject();

	// callback for login-attempt if user set "remember me"
	private final IAgoraCallback mLoginCallback = new IAgoraCallback() {
		@Override
		public void onSuccess(final String response) {
			try {
				currentAccount = Account.parseJson(new JSONObject(response)
						.getJSONObject("data"));

				Toast.makeText(MainActivity.getInstance(),
						"Logged in successfully", Toast.LENGTH_SHORT).show();
				Log.i(TAG, "Logged in as " + Settings.getUsername());
			} catch (JSONException e) {
				Log.e(TAG, "Error while parsing json", e);
				Toast.makeText(MainActivity.getInstance(),
						"An error occured when processing received data.",
						Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		public void onFailure(final RestException exception) {
			Log.e(TAG, "Error while logging into account", exception);

			Settings.clearCredentials();
			openActivity(LoginActivity.class);

			Toast.makeText(MainActivity.getInstance(),
					"Something unexpected happened, please try again later",
					Toast.LENGTH_SHORT).show();
		}
	};

	private IAgoraCallback mGetAllProductsCallback = new IAgoraCallback() {

		@Override
		public void onSuccess(String response) {
			try {
				final Product[] products = Product
						.parseProducts(new JSONObject(response)
								.getJSONObject("data"));

				Log.i(TAG, "Retrieved new products");

				mProductAdapter.setProducts(products);
				mProductGrid.invalidateViews();
				mProductGrid.setAdapter(mProductAdapter);
			} catch (JSONException e) {
				Log.e(TAG, "Error while parsing json", e);
				Toast.makeText(MainActivity.getInstance(),
						"An error occured when processing received data.",
						Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		public void onFailure(RestException exception) {
			Log.e(TAG, "Error while retrieving products", exception);

			Toast.makeText(MainActivity.getInstance(),
					"Could not load products, please try again later",
					Toast.LENGTH_SHORT).show();
		}
	};

	// static access to activity
	private static MainActivity mInstance;

	public static MainActivity getInstance() {
		return mInstance;
	}

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mInstance = this;

		getActionBar().setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);

		// check stored credentials
		if (Settings.getUsername() == null && Settings.getPassword() == null) {
			// user is not logged in, show login screen
			openActivity(LoginActivity.class);
		} else if (Settings.getUsername() != null
				&& Settings.getPassword() != null) {
			AgoraApi.login(mLoginCallback, Settings.getUsername(),
					Settings.getPassword());
		} else {
			// only one of username/pw is supplied, clear credentials and show
			// login screen
			Settings.clearCredentials();
			openActivity(LoginActivity.class);
		}

		// prepare product adapter
		mProductAdapter = new ProductAdapter(this);
		mProductGrid = ((GridView) findViewById(R.id.griAllProducts));
		mProductGrid.setAdapter(mProductAdapter);

		loadProducts();

		Log.d(TAG, TAG + " created.");
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_account_view) {
			if (currentAccount == null) {
				// no user is logged in, show login screen
				Toast.makeText(this, "Please log in first", Toast.LENGTH_SHORT)
						.show();
				openActivity(LoginActivity.class);
			} else {
				// show account screen if a user is logged in
				openActivity(AccountActivity.class);
			}
			return true;
		} else if (id == R.id.action_search_product) {
			addFilter();
			return true;
		} else if (id == R.id.action_create_product) {
			if (currentAccount == null) {
				// no user is logged in, show login screen
				Toast.makeText(this, "Please log in first", Toast.LENGTH_SHORT)
						.show();
				openActivity(LoginActivity.class);
			} else {
				// show product screen if a user is logged in
				openActivity(CreateProductActivity.class);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void addFilter() {
		try {
			String filterString = "";
			final Iterator<?> keys = mFilter.keys();
			while (keys.hasNext()) {
				final String key = (String) keys.next();
				filterString += "\n" + key + ":\t" + mFilter.getString(key);
			}

			new AlertDialog.Builder(this)
					.setTitle("Choose filter")
					.setMessage(
							filterString.isEmpty() ? "Currently no filters present"
									: "Current filters:" + filterString)
					.setPositiveButton("Add",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									chooseNewFilterType();
								}
							})
					.setNegativeButton("Clear",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									// clear filter
									mFilter = new JSONObject();

									// retrieve products
									loadProducts();

									Log.i(TAG, "Cleared filter");
								}
							}).show();
		} catch (JSONException e) {
			Log.e(TAG, "Another JSON error that will never happen...", e);
		}
	}

	private void chooseNewFilterType() {
		new AlertDialog.Builder(this)
				.setSingleChoiceItems(ProductFilterType.getStringValues(), -1,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								dialog.dismiss();
								int selectedPosition = ((AlertDialog) dialog)
										.getListView().getCheckedItemPosition();
								chooseNewFilterValue(ProductFilterType
										.fromValue(selectedPosition));
							}
						})
				.setTitle("Filter Type")
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface arg0, int arg1) {
								// do nothing
							}
						}).show();
	}

	private void chooseNewFilterValue(final ProductFilterType type) {
		// Set an EditText view to get user input
		final EditText input = new EditText(this);
		if (type == ProductFilterType.MinPrice
				|| type == ProductFilterType.MaxPrice) {
			input.setInputType(InputType.TYPE_CLASS_NUMBER
					| InputType.TYPE_NUMBER_FLAG_DECIMAL);
		}

		new AlertDialog.Builder(this)
				.setTitle("Filter Value")
				.setMessage("Filter " + type + " with what value?")
				.setView(input)
				.setPositiveButton("Add",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								final String value = input.getText().toString();
								try {
									// add to filter
									mFilter.put(
											type.toString()
													.substring(0, 1)
													.toLowerCase(
															getResources()
																	.getConfiguration().locale)
													+ type.toString()
															.substring(1),
											value);

									Log.d(TAG, "Added filter with type " + type
											+ " and value " + value);

									loadProducts();
								} catch (JSONException e) {
									Log.e(TAG, "ERROR while putting to json");
								}
							}
						})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// do nothing
							}
						}).show();
	}

	public void loadProducts() {
		// retrieve products
		AgoraApi.getAllProducts(mGetAllProductsCallback, mFilter);
	}

	@Override
	protected void onResume() {
		super.onResume();

		// TODO: set action bar items?
	}

	// overload for data-less intents
	private void openActivity(final Class<?> activity) {
		openActivity(activity, null, null);
	}

	// starts new intent with the given class and data
	private void openActivity(final Class<?> activity, final String name,
			final String data) {
		final Intent intent = new Intent(this, activity);
		if (name != null && data != null) {
			intent.putExtra(name, data);
		}
		startActivity(intent);
	}
}
