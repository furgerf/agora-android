package coventry.university.ac.uk.agora.activity;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import coventry.university.ac.uk.R;
import coventry.university.ac.uk.agora.common.Settings;
import coventry.university.ac.uk.agora.documents.Account;
import coventry.university.ac.uk.agora.rest.AgoraApi;
import coventry.university.ac.uk.agora.rest.IAgoraCallback;
import coventry.university.ac.uk.agora.rest.RestException;

/**
 * Activity that shows a screen for logging in/creating an account.
 * 
 * @author fabian
 *
 */
public final class LoginActivity extends Activity {
	private final static String TAG = LoginActivity.class.getSimpleName();

	// views
	private EditText mTxtUsername;
	private EditText mTxtPassword;
	private CheckBox mChkRememberMe;

	// callbacks
	private final IAgoraCallback mCreateAccountCallback = new IAgoraCallback() {
		@Override
		public void onSuccess(final String response) {
			Log.i(TAG, "Account created");
			Toast.makeText(MainActivity.getInstance(),
					"The account was created successfully!", Toast.LENGTH_SHORT)
					.show();

			// login
			login(mTxtUsername.getText().toString(), mTxtPassword.getText()
					.toString());
		}

		@Override
		public void onFailure(final RestException exception) {
			if (exception.isRestException()
					&& exception.code.equals("AccountAlreadyExists")) {
				Log.i(TAG,
						"Account couldn't be created because the email already exists");
				Toast.makeText(MainActivity.getInstance(),
						"An account with this email already exists",
						Toast.LENGTH_SHORT).show();
			} else if (exception.isRestException()
					&& exception.code.equals("InvalidProperties")) {
				Log.i(TAG,
						"Account couldn't be created because properties are invalid");
				Toast.makeText(MainActivity.getInstance(),
						"Please provide a valid email address",
						Toast.LENGTH_SHORT).show();
			} else if (exception.isRestException()
					&& exception.code.equals("BasicHTTPAuthRequired")) {
				Log.i(TAG,
						"Account couldn't be created because no username/password were provided");
				Toast.makeText(MainActivity.getInstance(),
						"Please provide a valid email address and a password",
						Toast.LENGTH_SHORT).show();
			} else {
				Log.e(TAG, "Error while creating account", exception);
				Toast.makeText(
						MainActivity.getInstance(),
						"Something unexpected happened, please try again later",
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	private final IAgoraCallback mLoginCallback = new IAgoraCallback() {
		@Override
		public void onSuccess(final String response) {
			// store credentials(?)
			Settings.setUsername(mTxtUsername.getText().toString(),
					mChkRememberMe.isChecked());
			Settings.setPassword(mTxtPassword.getText().toString(),
					mChkRememberMe.isChecked());

			try {
				final Account account = Account.parseJson(new JSONObject(
						response).getJSONObject("data"));
				MainActivity.getInstance().currentAccount = account;

				Toast.makeText(MainActivity.getInstance(),
						"Logged in successfully", Toast.LENGTH_SHORT).show();
				Log.i(TAG, "Logged in as " + mTxtUsername.getText());

				finish();
			} catch (JSONException e) {
				Log.e(TAG, "Error while parsing json", e);
				Toast.makeText(MainActivity.getInstance(),
						"An error occured when processing received data.",
						Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		public void onFailure(final RestException exception) {
			if (exception.isRestException()
					&& (exception.code.equals("InvalidUsernameOrPassword")
							|| exception.code.equals("BasicHTTPAuthRequired") || exception.code
								.equals("AccountNotFound"))) {
				Log.i(TAG, "Invalid username or password");
				Toast.makeText(
						MainActivity.getInstance(),
						"Could not log in because username or passwort is wrong",
						Toast.LENGTH_SHORT).show();
			} else {
				Log.e(TAG, "Error while logging into account", exception);
				Toast.makeText(
						MainActivity.getInstance(),
						"Something unexpected happened, please try again later",
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		// assign views
		mTxtUsername = (EditText) findViewById(R.id.txtUsername);
		mTxtPassword = (EditText) findViewById(R.id.txtPassword);
		mChkRememberMe = (CheckBox) findViewById(R.id.chkRememberLogin);

		Log.d(TAG, TAG + " created.");
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_main_view) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// method that is called by the button
	public void loginAccount(final View view) {
		Log.i(TAG, "Attempting to log in to account " + mTxtUsername.getText());

		login(mTxtUsername.getText().toString(), mTxtPassword.getText()
				.toString());
	}

	// method that is called by the button
	public void registerAccount(final View view) {
		Log.i(TAG, "Attempting to create account " + mTxtUsername.getText());

		AgoraApi.createAccount(mCreateAccountCallback, mTxtUsername.getText()
				.toString(), mTxtPassword.getText().toString());
	}

	// actually make the API login call
	private void login(final String username, final String password) {
		AgoraApi.login(mLoginCallback, username, password);
	}
}
