package coventry.university.ac.uk.agora.common;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Extends the ImageView to always display the image scaled with correct aspect
 * ratio.
 * 
 * @author http://stackoverflow.com/a/18077714/2135142, modified by fabian
 *
 */
public final class ScaledImageView extends ImageView {

	public ScaledImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		try {
			final Drawable drawable = getDrawable();

			if (drawable == null) {
				setMeasuredDimension(0, 0);
			} else {
				final float imageSideRatio = (float) drawable
						.getIntrinsicWidth()
						/ (float) drawable.getIntrinsicHeight();
				final float viewSideRatio = (float) MeasureSpec
						.getSize(widthMeasureSpec)
						/ (float) MeasureSpec.getSize(heightMeasureSpec);
				if (imageSideRatio >= viewSideRatio) {
					// Image is wider than the display (ratio)
					int width = MeasureSpec.getSize(widthMeasureSpec);
					final int height = (int) (width / imageSideRatio);

					// if the image is too tall, make it half-sized so it doesn't fill the whole screen
					if (imageSideRatio <= 1) {
						width /= 2;
					}
					setMeasuredDimension(width, height);
				} else {
					// Image is taller than the display (ratio)
					final int height = MeasureSpec.getSize(heightMeasureSpec);
					final int width = (int) (height * imageSideRatio);
					setMeasuredDimension(width, height);
				}
			}
		} catch (Exception e) {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}
	}
}
