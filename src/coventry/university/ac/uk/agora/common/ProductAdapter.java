package coventry.university.ac.uk.agora.common;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import coventry.university.ac.uk.R;
import coventry.university.ac.uk.agora.activity.MainActivity;
import coventry.university.ac.uk.agora.activity.ViewProductActivity;
import coventry.university.ac.uk.agora.documents.Product;

public class ProductAdapter extends BaseAdapter {
	private final LayoutInflater inflater;

	private Product[] mProducts = new Product[0];

	public ProductAdapter(final Context context) {
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	// create a new ImageView for each item referenced by the Adapter
	public View getView(final int position, final View convertView,
			final ViewGroup parent) {
		final View productView;
		if (convertView == null) {
			// if it's not recycled, initialize some attributes
			productView = inflater.inflate(R.layout.product, null);

			final ImageView img = (ImageView) productView
					.findViewById(R.id.imgProduct);
			final TextView txt = (TextView) productView
					.findViewById(R.id.txtProduct);

			productView.setPadding(8, 8, 8, 8);

			txt.setText(mProducts[position].name);
			if (mProducts[position].images == null) {
				img.setImageResource(R.drawable.agora_logo);
			} else {
				final byte[] decodedByte = Base64.decode(
						mProducts[position].images[0], 0);
				img.setImageBitmap(BitmapFactory.decodeByteArray(decodedByte,
						0, decodedByte.length));
			}

			productView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					final Intent intent = new Intent(
							MainActivity.getInstance(),
							ViewProductActivity.class);
					intent.putExtra("product", mProducts[position].getJson());
					MainActivity.getInstance().startActivity(intent);
				}
			});

		} else {
			productView = (LinearLayout) convertView;
		}

		return productView;
	}

	@Override
	public int getCount() {
		return mProducts.length;
	}

	@Override
	public Object getItem(int position) {
		return mProducts[position];
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	public void setProducts(final Product[] products) {
		mProducts = products == null ? new Product[0] : products;
		notifyDataSetChanged();
	}
}
