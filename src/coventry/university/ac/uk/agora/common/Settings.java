package coventry.university.ac.uk.agora.common;

import coventry.university.ac.uk.agora.activity.MainActivity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

/**
 * Class that is used to store/retrieve settings. Only used for user credentials
 * here.
 * 
 * @author fabian
 *
 */
public final class Settings {
	// constant values
	private final static SharedPreferences mSharedPrefs = PreferenceManager
			.getDefaultSharedPreferences(MainActivity.getInstance());

	private final static String KEY_USER = "user";
	private final static String KEY_PASSWORD = "password";

	private static String mUsername;
	private static String mPassword;

	private Settings() {
		// don't instantiate
	}

	// setter
	public static void setUsername(final String username, final boolean remember) {
		mUsername = username;

		if (remember) {
			final Editor editor = mSharedPrefs.edit();
			editor.putString(KEY_USER, username);
			editor.commit();
		}
	}

	public static void setPassword(final String password, final boolean remember) {
		mPassword = password;

		if (remember) {
			final Editor editor = mSharedPrefs.edit();
			editor.putString(KEY_PASSWORD, password);
			editor.commit();
		}
	}

	// getter
	public static String getUsername() {
		if (mUsername == null) {
			mUsername = mSharedPrefs.getString(KEY_USER, null);
		}

		return mUsername;
	}

	public static String getPassword() {
		if (mPassword == null) {
			mPassword = mSharedPrefs.getString(KEY_PASSWORD, null);
		}

		return mPassword;
	}

	// clear credentials
	public static void clearCredentials() {
		final Editor editor = mSharedPrefs.edit();
		editor.remove(KEY_USER);
		editor.remove(KEY_PASSWORD);
		editor.commit();
	}
}