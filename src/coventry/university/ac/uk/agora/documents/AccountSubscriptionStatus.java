package coventry.university.ac.uk.agora.documents;

/**
 * The account subscription status values.
 * 
 * @author fabian
 *
 */
public enum AccountSubscriptionStatus {
	Free(0), Private(1), Business(2);

	private final int mValue;

	private AccountSubscriptionStatus(final int value) {
		this.mValue = value;
	}

	public int getValue() {
		return mValue;
	}
}
