package coventry.university.ac.uk.agora.documents;

public enum OfferStatus {
	Offered(0), Accepted(1), Rejected(2), Paid(3), Withdrawn(4);

	private final int mValue;

	private OfferStatus(final int value) {
		this.mValue = value;
	}

	public int getValue() {
		return mValue;
	}
}
