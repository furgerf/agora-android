package coventry.university.ac.uk.agora.documents;

import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import coventry.university.ac.uk.agora.activity.MainActivity;

/**
 * Represents an account as used by the Agora API.
 * 
 * @author fabian
 *
 */
public final class Account extends Document {
	private final static String TAG = Account.class.getSimpleName();

	// fields
	public final String _id;
	public final String email;
	public final String firstName;
	public final String lastName;
	public final AccountSubscriptionStatus subscriptionStatus;
	public final Date subscriptionExpiration;
	public final Date lastModified;

	// constructor
	private Account(final String _id, final String email,
			final String firstName, final String lastName,
			final AccountSubscriptionStatus subscriptionStatus,
			final Date subscriptionExpiration, final Date lastModified) {
		this._id = _id;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.subscriptionStatus = subscriptionStatus;
		this.subscriptionExpiration = subscriptionExpiration;
		this.lastModified = lastModified;
	}

	// de-serialization
	public static Account parseJson(final JSONObject json) {
		try {
			final String _id = json.has("_id") ? json.getString("_id") : null;
			final String email = json.has("email") ? json.getString("email")
					: null;
			final String firstName = json.has("firstName") ? json
					.getString("firstName") : null;
			final String lastName = json.has("lastName") ? json
					.getString("lastName") : null;
			final JSONObject subscription = json.has("subscription") ? json
					.getJSONObject("subscription") : null;
			AccountSubscriptionStatus subscriptionStatus = null;
			Date subscriptionExpiration = null;
			if (subscription != null) {
				subscriptionStatus = subscription.has("status") ? AccountSubscriptionStatus
						.valueOf(subscription
								.getString("status")
								.substring(0, 1)
								.toUpperCase(
										MainActivity.getInstance()
												.getResources()
												.getConfiguration().locale)
								+ subscription.getString("status").substring(1))
						: null;
				subscriptionExpiration = subscription.has("expiration")
						&& subscription.getString("expiration") != "null" ? parseIsoDate(subscription
						.getString("expiration")) : null;
			}
			final Date lastModified = json.has("lastModified") ? parseIsoDate(json
					.getString("lastModified")) : null;

			return new Account(_id, email, firstName, lastName,
					subscriptionStatus, subscriptionExpiration, lastModified);
		} catch (JSONException e) {
			Log.e(TAG, "Error parsing Account JSON: " + json, e);
			return null;
		}
	}

	// serialization
	public String getJson() {
		try {
			final JSONObject json = new JSONObject();
			if (_id != null) {
				json.put("_id", _id);
			}
			if (email != null) {
				json.put("email", email);
			}
			if (firstName != null) {
				json.put("firstName", firstName);
			}
			if (lastName != null) {
				json.put("lastName", lastName);
			}
			final JSONObject subscription = new JSONObject();
			if (subscriptionStatus != null) {
				subscription.put("status", subscriptionStatus);
			}
			if (subscriptionExpiration != null) {
				subscription.put("expiration",
						getIsoDateString(subscriptionExpiration));
			}
			json.put("subscription", subscription);
			if (lastModified != null) {
				json.put("lastModified", getIsoDateString(lastModified));
			}
			return json.toString();
		} catch (JSONException e) {
			Log.e(TAG, "Error while stringifying json", e);
			return null;
		}
	}
}
