package coventry.university.ac.uk.agora.documents;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;
import coventry.university.ac.uk.agora.activity.MainActivity;

/**
 * Parent class for classes that represent documents in the database as used by
 * the Agora API. Provides commonly usefuul methods
 * 
 * @author fabian
 *
 */
public abstract class Document {
	private final static String TAG = Document.class.getSimpleName();
	private final static SimpleDateFormat ISO_FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", MainActivity.getInstance()
					.getResources().getConfiguration().locale);

	// string to ISO 8061 date
	public static Date parseIsoDate(final String dateString) {
		try {
			return ISO_FORMAT.parse(dateString);
		} catch (ParseException e) {
			Log.e(TAG, "ERROR PARSING ISO DATE: " + dateString, e);
			return null;
		}
	}

	// ISO 8061 date to string
	public static String getIsoDateString(final Date date) {
		return ISO_FORMAT.format(date);
	}

	// json array to string array
	public static String[] jsonArrayToStringArray(final JSONArray json) {
		try {
			final String[] result = new String[json.length()];
			for (int i = 0; i < json.length(); i++) {

				result[i] = json.getString(i);
			}
			return result;
		} catch (JSONException e) {
			Log.e(TAG, "ERROR PARSING JSON ARRAY", e);
			return null;
		}
	}

	// string array to json array
	public static JSONArray stringListToJsonArray(final List<String> list) {
		final JSONArray result = new JSONArray();

		for (final String s : list) {
			result.put(s);
		}

		return result;
	}
}
