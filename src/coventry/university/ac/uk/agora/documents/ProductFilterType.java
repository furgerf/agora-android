package coventry.university.ac.uk.agora.documents;

public enum ProductFilterType {
	Name(0), Tags(1), Description(2), MinPrice(3), MaxPrice(4);

	private final int mValue;

	private ProductFilterType(final int value) {
		this.mValue = value;
	}

	public static ProductFilterType fromValue(final int value) {
		return ProductFilterType.values()[value];
	}

	public int getValue() {
		return mValue;
	}

	public static String[] getStringValues() {
		final ProductFilterType[] values = ProductFilterType.values();
		final String[] result = new String[values.length];

		for (int i = 0; i < values.length; i++) {
			result[i] = values[i].toString();
		}

		return result;
	}
}
