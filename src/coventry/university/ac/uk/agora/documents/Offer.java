package coventry.university.ac.uk.agora.documents;

import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import coventry.university.ac.uk.agora.activity.MainActivity;

/**
 * Represents a offer as used by the Agora API.
 * 
 * @author fabian
 *
 */
public final class Offer extends Document {
	private final static String TAG = Offer.class.getSimpleName();

	// fields
	public final String _id;
	public final String seller;
	public final String buyer;
	public final String product;
	public final Double price;
	public final String description;
	public final OfferStatus status;
	// ignore payment
	public final Date lastModified;
	public final Date startDate;
	public final Date expirationDate;

	// constructor
	private Offer(final String _id, final String seller, final String buyer,
			final String product, final Double price, final String description,
			final OfferStatus status, final Date lastModified,
			final Date startDate, final Date expirationDate) {
		this._id = _id;
		this.seller = seller;
		this.buyer = buyer;
		this.product = product;
		this.price = price;
		this.description = description;
		this.status = status;
		this.lastModified = lastModified;
		this.startDate = startDate;
		this.expirationDate = expirationDate;
	}

	public static Offer[] parseOffers(final JSONObject json) {
		try {
			final JSONArray array = json.getJSONArray("items");
			if (array.length() == 0) {
				return new Offer[0];
			}

			final Offer[] result = new Offer[array.length()];

			for (int i = 0; i < array.length(); i++) {
				result[i] = parseJson(array.getJSONObject(i));

				if (result[i] == null) {
					throw new JSONException("ERROR WHILE PARSING OFFER");
				}
			}

			return result;
		} catch (JSONException e) {
			Log.e(TAG, "Error while parsing offer array", e);
			return null;
		}
	}

	// de-serialization
	public static Offer parseJson(final JSONObject json) {
		try {
			final String _id = json.has("_id") ? json.getString("_id") : null;
			final String description = json.has("description") ? json
					.getString("description") : null;
			final String buyer;
			if (json.has("buyer")) {
				if (json.get("buyer") instanceof JSONObject) {
					if (json.getJSONObject("buyer").has("_id")) {
						buyer = json.getJSONObject("buyer").getString("_id");
					} else {
						throw new JSONException("Unexpected JSON format");
					}
				} else {
					buyer = json.getString("buyer");
				}
			} else {
				buyer = null;
			}
			final String seller;
			if (json.has("seller")) {
				if (json.get("seller") instanceof JSONObject) {
					if (json.getJSONObject("seller").has("_id")) {
						seller = json.getJSONObject("seller").getString("_id");
					} else {
						throw new JSONException("Unexpected JSON format");
					}
				} else {
					seller = json.getString("seller");
				}
			} else {
				seller = null;
			}
			final String product;
			if (json.has("product")) {
				if (json.get("product") instanceof JSONObject) {
					if (json.getJSONObject("product").has("_id")) {
						product = json.getJSONObject("product")
								.getString("_id");
					} else {
						throw new JSONException("Unexpected JSON format");
					}
				} else {
					product = json.getString("product");
				}
			} else {
				product = null;
			}
			final OfferStatus status = json.has("status") ? OfferStatus
					.valueOf(json
							.getString("status")
							.substring(0, 1)
							.toUpperCase(
									MainActivity.getInstance().getResources()
											.getConfiguration().locale)
							+ json.getString("status").substring(1)) : null;

			final Double price = json.has("price") ? json.getDouble("price")
					: null;
			final Date lastModified = json.has("lastModified") ? parseIsoDate(json
					.getString("lastModified")) : null;
			final Date startDate = json.has("startDate") ? parseIsoDate(json
					.getString("startDate")) : null;
			final Date expirationDate = json.has("expirationDate") ? parseIsoDate(json
					.getString("expirationDate")) : null;

			return new Offer(_id, seller, buyer, product, price, description,
					status, lastModified, startDate, expirationDate);
		} catch (JSONException e) {
			Log.e(TAG, "Error parsing offer JSON: " + json, e);
			return null;
		}
	}

	// serialization
	// public String getJson() {
	// try {
	// final JSONObject json = new JSONObject();
	// if (name != null) {
	// json.put("name", name);
	// }
	// if (description != null) {
	// json.put("description", description);
	// }
	// if (owner != null) {
	// json.put("owner", owner);
	// }
	// if (_id != null) {
	// json.put("_id", _id);
	// }
	// if (minPrice != null) {
	// json.put("minPrice", minPrice);
	// }
	// if (maxPrice != null) {
	// json.put("maxPrice", maxPrice);
	// }
	// if (tags != null) {
	// json.put("tags", new JSONArray(tags));
	// }
	// if (images != null) {
	// json.put("images", new JSONArray(images));
	// }
	// if (lastModified != null) {
	// json.put("lastModified", getIsoDateString(lastModified));
	// }
	// if (startDate != null) {
	// json.put("startDate", getIsoDateString(startDate));
	// }
	// if (expirationDate != null) {
	// json.put("expirationDate", getIsoDateString(expirationDate));
	// }
	// return json.toString();
	// } catch (JSONException e) {
	// Log.e(TAG, "Error while stringifying json", e);
	// return null;
	// }
	// }
}
