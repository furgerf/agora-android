package coventry.university.ac.uk.agora.documents;

import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import coventry.university.ac.uk.agora.common.Settings;
import coventry.university.ac.uk.agora.rest.AgoraApi;
import android.util.Log;

/**
 * Represents a product as used by the Agora API.
 * 
 * @author fabian
 *
 */
public final class Product extends Document {
	private final static String TAG = Product.class.getSimpleName();

	// fields
	public final String _id;
	public final String name;
	public final String description;
	public final String[] tags;
	public final String owner;
	public final String[] images;
	public final Double minPrice;
	public final Double maxPrice;
	public final Date lastModified;
	public final Date startDate;
	public final Date expirationDate;

	// constructor
	private Product(final String _id, final String name,
			final String description, final String[] tags, final String owner,
			final String[] images, final Double minPrice,
			final Double maxPrice, final Date lastModified,
			final Date startDate, final Date expirationDate) {
		this._id = _id;
		this.name = name;
		this.description = description;
		this.tags = tags;
		this.owner = owner;
		this.images = images;
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
		this.lastModified = lastModified;
		this.startDate = startDate;
		this.expirationDate = expirationDate;
	}

	public static Product[] parseProducts(final JSONObject json) {
		try {
			final JSONArray array = json.getJSONArray("items");
			if (array.length() == 0) {
				return new Product[0];
			}

			final Product[] result = new Product[array.length()];

			for (int i = 0; i < array.length(); i++) {
				result[i] = parseJson(array.getJSONObject(i));

				if (result[i] == null) {
					throw new JSONException("ERROR WHILE PARSING PRODUCT");
				}
			}

			return result;
		} catch (JSONException e) {
			Log.e(TAG, "Error while parsing product array", e);
			return null;
		}
	}

	// de-serialization
	public static Product parseJson(final JSONObject json) {
		try {
			final String _id = json.has("_id") ? json.getString("_id") : null;
			final String name = json.has("name") ? json.getString("name")
					: null;
			final String description = json.has("description") ? json
					.getString("description") : null;
			final String owner;
			if (json.has("owner")) {
				if (json.get("owner") instanceof JSONObject) {
					if (json.getJSONObject("owner").has("_id")) {
						owner = json.getJSONObject("owner").getString("_id");
					} else {
						throw new JSONException("Unexpected JSON format");
					}
				} else {
					owner = json.getString("owner");
				}
			} else {
				owner = null;
			}
			final String[] tags = json.has("tags") ? jsonArrayToStringArray(json
					.getJSONArray("tags")) : null;
			final String[] images = json.has("images") ? jsonArrayToStringArray(json
					.getJSONArray("images")) : null;
			final Double minPrice = json.has("minPrice") ? json
					.getDouble("minPrice") : null;
			final Double maxPrice = json.has("maxPrice") ? json
					.getDouble("maxPrice") : null;
			final Date lastModified = json.has("lastModified") ? parseIsoDate(json
					.getString("lastModified")) : null;
			final Date startDate = json.has("startDate") ? parseIsoDate(json
					.getString("startDate")) : null;
			final Date expirationDate = json.has("expirationDate") ? parseIsoDate(json
					.getString("expirationDate")) : null;

			return new Product(_id, name, description, tags, owner, images,
					minPrice, maxPrice, lastModified, startDate, expirationDate);
		} catch (JSONException e) {
			Log.e(TAG, "Error parsing product JSON: " + json, e);
			return null;
		}
	}

	// serialization
	public String getJson() {
		try {
			final JSONObject json = new JSONObject();
			if (name != null) {
				json.put("name", name);
			}
			if (description != null) {
				json.put("description", description);
			}
			if (owner != null) {
				json.put("owner", owner);
			}
			if (_id != null) {
				json.put("_id", _id);
			}
			if (minPrice != null) {
				json.put("minPrice", minPrice);
			}
			if (maxPrice != null) {
				json.put("maxPrice", maxPrice);
			}
			if (tags != null) {
				json.put("tags", new JSONArray(tags));
			}
			if (images != null) {
				json.put("images", new JSONArray(images));
			}
			if (lastModified != null) {
				json.put("lastModified", getIsoDateString(lastModified));
			}
			if (startDate != null) {
				json.put("startDate", getIsoDateString(startDate));
			}
			if (expirationDate != null) {
				json.put("expirationDate", getIsoDateString(expirationDate));
			}
			return json.toString();
		} catch (JSONException e) {
			Log.e(TAG, "Error while stringifying json", e);
			return null;
		}
	}

	public boolean isOwnedByCurrentAccount() {
		return owner != null
				&& AgoraApi.getSha1Hash(Settings.getUsername()).equals(owner);
	}
}
